//
//  ViewController.swift
//  TicTacToe v1.1
//
//  Created by Ed.Bujak on 2/4/19.
//  Copyright © 2019 Ed.Bujak. All rights reserved.
//

import UIKit

var grid: [[Int]] = [];
var currentPlayer = false;
var awaitingReset = false;
var accessed: [UIButton] = [];

class ViewController: UIViewController {
    @IBOutlet weak var currentPlayerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        newGame();
    }
    
    func checkForWinner() -> Bool {
        // check for straight-across
        for i in 0..<grid.count {
            // Sets can only have one of each value
            if (Set(grid[i]).count == 1) && (grid[i][0] != 0) {
                return true;
            }
        }
        
        // check for straight-vertical
        for i in 0...2 {
            if (grid[0][i], grid[1][i]) == (grid[1][i], grid[2][i]) && grid[1][i] != 0 {
                return true;
            }
        }
        
        // check for diagonal
        if (grid[0][0], grid[1][1]) == (grid[1][1], grid[2][2]) && grid[1][1] != 0 {
            return true;
        }
        if (grid[2][0], grid[1][1]) == (grid[1][1], grid[0][2]) && grid[1][1] != 0 {
            return true;
        }
        
        return false;
    }

    func changePlayer() {
        currentPlayer = !currentPlayer;
        currentPlayerLabel.text = currentPlayer ? "Current Player: 1" : "Current Player: 2";
    }
    
    func newGame() {
        currentPlayer = false;
        awaitingReset = false;
        grid = [
            [0,0,0],
            [0,0,0],
            [0,0,0]
        ];
        for btn in accessed {
            btn.setImage(nil, for: .normal);
        }
        accessed = [];
        changePlayer();
    }
    
    func tieGame() -> Bool {
        for row in grid {
            for col in row {
                if col == 0 {
                    return false;
                }
            }
        }
        return true;
    }
    
    @IBAction func tappedOnButton(_ sender: UIButton) {
        if (awaitingReset) {
            newGame();
            return;
        }

        let location = sender.accessibilityIdentifier!.split(separator: ",");
        
        let row: Int = Int(location[0])!;
        let col: Int = Int(location[1])!;
        
        if (grid[row][col] != 0) {
            return;
        }

        accessed.append(sender);
        
        if (currentPlayer) {
            grid[row][col] = 1;
            sender.setImage(UIImage(named: "x.png"), for: .normal);
        } else {
            grid[row][col] = 2;
            sender.setImage(UIImage(named: "o.png"), for: .normal);
        }

        if checkForWinner() {
            var newLabelText = currentPlayer ? "Player 1 Wins!" : "Player 2 Wins!";
            
            newLabelText += " Tap on the grid to reset.";
            
            currentPlayerLabel.text = newLabelText;
            awaitingReset = true;

        } else if tieGame() {
            currentPlayerLabel.text = "Tie Game! Tap on the grid to reset.";
            awaitingReset = true;
        } else {
            changePlayer();
        }
    }
}

